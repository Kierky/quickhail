-- Lovingly coded by Jasmine Atamoss.
-- Angrily re-coded by Kierky
-- Commands:
-- /qh or /quickhail - Display the config dialog.
-- 
-- /qhlist - Display the Hail list, rebinds the number keys and awaits input. Once hail is selected, hail is sent to VO
-- hail list is closed, and the number keys are returned back to their prior state.

declare('quickhail', {})

quickhail.version = '2.0'

quickhail.settingsnote = 2748264821
quickhail.settingslist = {}

-- a few functions for systemnotes manipulation

function quickhail.updatenotes()
	quickhail.savenote(quickhail.settingslist, quickhail.settingsnote)
	quickhail.loadnotes()
end

function quickhail.savenote(tbl, systemnotesid)
	if (type(tbl) ~= 'table') then return nil, 'Table expected, got '..type(tbl)..'.' end
	return pcall(SaveSystemNotes, spickle(tbl), systemnotesid)
end

function quickhail.loadnotes() 
	quickhail.settingslist = unspickle(LoadSystemNotes(quickhail.settingsnote)) or {}
end

quickhail.loadnotes()

dofile('vars.lua')
dofile('ui.lua')

-- START HAIL SCRIPT

function quickhail.hailgo(args)
	if quickhail.var["hail"..args] == "" then
		print(args.." has not been set.")
	elseif quickhail.var["hail"..args] ~= "" then
		gkinterface.GKProcessCommand(quickhail.var["hailtype"..args] .. " " .. quickhail.var["hail"..args])
	end
	quickhail.hidemain()
	quickhail.display.detachall()
end

function quickhail.loadvariables()
	if quickhail.var.hailsloaded == 'no' then
		quickhail.loadvarscript()
	elseif quickhail.charid ~= GetCharacterID() then
		quickhail.charid = GetCharacterID()
		quickhail.loadvarscript()
	end
	quickhail.charid = GetCharacterID()
end

function quickhail.loadvarscript()
	do
		quickhail.updatenotes()
		local charid = GetCharacterID()
		for i=1,10 do
			local key = charid..'hailtype'..i
			local val = "msg %target%"
			quickhail.var['hailtype'..i] = quickhail.settingslist[key] or val
			if quickhail.var["hailtype"..i] == "msg %target%" then
				quickhail.var['hailtypetf'..i] = "OFF"
				quickhail.ui["hailtype"..i].value = "OFF"
			elseif quickhail.var["hailtype"..i] == "say_sector" then
				quickhail.var['hailtypetf'..i] = "ON"
				quickhail.ui["hailtype"..i].value = "ON"
			end
			local key2 = charid..'hail'..i
			local val2 = "Hail!"
			quickhail.var['hail'..i] = quickhail.settingslist[key2] or val2
			quickhail.ui['hailname'..i].value = quickhail.var['hail'..i]
			quickhail.ui['haildisplay'..i].title = quickhail.var['hail'..i]
		end
		quickhail.ui.hailtypeset()
		quickhail.var.hailsloaded = 'yes'
	end
end

function quickhail.showhails()
	quickhail.updatenotes()
	quickhail.loadvariables()
	if quickhail.ui.active == false then
		quickhail.display.qhliston()
		quickhail.showmain()
		quickhail.currentmode = "hail"
	elseif quickhail.ui.active == true then
		quickhail.hidemain()
		quickhail.display.detachall()
		quickhail.currentmode = ""
	end
end

RegisterUserCommand('qhlist', quickhail.showhails)
-- END HAIL SCRIPT

-- START KEY BIND/REBIND/PRESS

-- Crash Protection

for i=0,9 do
	quickhail.keyvar[i] = gkinterface.GetCommandForKeyboardBind(gkinterface.GetInputCodeByName(i))
	local pkey = 'crashprotectdefault' ..i
	local pval = ''
	quickhail.default[i] = quickhail.settingslist[pkey] or pval
	if quickhail.keyvar[i] == 'qhkey'..i then
		gkinterface.BindCommand(gkinterface.GetInputCodeByName(i), quickhail.default[i])
		print('QuickHail Crash Protection: Key '..i..' returned to Default. ('..quickhail.default[i]..')')
		quickhail.keyvar[i] = quickhail.default[i]
		local key = 'crashprotectdefault' ..i
		local val = quickhail.keyvar[i]
		quickhail.settingslist[key] = value
	else
		local key = 'crashprotectdefault' ..i
		local val = quickhail.keyvar[i]
		quickhail.settingslist[key] = value
	end
end

-- End Crash Protection

function quickhail.keypress(args)
	quickhail.var.keypress = args
	if quickhail.currentmode == "hail" then
		quickhail.hailgo(args)
	end
end

function quickhail.setkeys()
	for i=0,9 do
		quickhail.keyvar[i] = gkinterface.GetCommandForKeyboardBind(gkinterface.GetInputCodeByName(i))
		gkinterface.BindCommand(gkinterface.GetInputCodeByName(i), 'qhkey'..i)
	end
end

function quickhail.resetkeys()
	for i=0,9 do
		gkinterface.BindCommand(gkinterface.GetInputCodeByName(i), quickhail.keyvar[i])
	end
end

RegisterUserCommand('qhkey1', function() quickhail.keypress("1") end)
RegisterUserCommand('qhkey2', function() quickhail.keypress("2") end)
RegisterUserCommand('qhkey3', function() quickhail.keypress("3") end)
RegisterUserCommand('qhkey4', function() quickhail.keypress("4") end)
RegisterUserCommand('qhkey5', function() quickhail.keypress("5") end)
RegisterUserCommand('qhkey6', function() quickhail.keypress("6") end)
RegisterUserCommand('qhkey7', function() quickhail.keypress("7") end)
RegisterUserCommand('qhkey8', function() quickhail.keypress("8") end)
RegisterUserCommand('qhkey9', function() quickhail.keypress("9") end)
RegisterUserCommand('qhkey0', function() quickhail.keypress("10") end)

-- END KEY BIND/REBIND/PRESS

quickhail.updatenotes()