-- START DISPLAY UI

for i=1,5 do
	local odds = i*2-1
	local evens = i*2
	quickhail.ui['haildisplay' .. odds] = iup.label{ title = '', fgcolor = '255 255 255', font = quickhail.var.font}
	quickhail.ui['haildisplay' .. evens] = iup.label{ title = '', fgcolor = '200 200 220', font = quickhail.var.font}
	quickhail.ui['hailnumdisplay' .. odds] = iup.label{ title = odds..'. ', fgcolor = '255 255 255', font = quickhail.var.font}
	quickhail.ui['hailnumdisplay' .. evens] = iup.label{ title = evens..'. ', fgcolor = '180 180 180', font = quickhail.var.font}
end

for i=1,10 do
	quickhail.ui['hailtypedisplay'..i] = iup.label{ title = '', fgcolor = '255 255 255', font = quickhail.var.font}
end

quickhail.ui.hailtitle = iup.label{title = 'Select A Hail:\n', fgcolor = '255 255 255', font = quickhail.var.font}
quickhail.ui.displaylinenum = iup.vbox{
	quickhail.ui.hailnumdisplay1,
	quickhail.ui.hailnumdisplay2,
	quickhail.ui.hailnumdisplay3,
	quickhail.ui.hailnumdisplay4,
	quickhail.ui.hailnumdisplay5,
	quickhail.ui.hailnumdisplay6,
	quickhail.ui.hailnumdisplay7,
	quickhail.ui.hailnumdisplay8,
	quickhail.ui.hailnumdisplay9,
	quickhail.ui.hailnumdisplay10
}
quickhail.ui.displaylinetype = iup.vbox{
	quickhail.ui.hailtypedisplay1,
	quickhail.ui.hailtypedisplay2,
	quickhail.ui.hailtypedisplay3,
	quickhail.ui.hailtypedisplay4,
	quickhail.ui.hailtypedisplay5,
	quickhail.ui.hailtypedisplay6,
	quickhail.ui.hailtypedisplay7,
	quickhail.ui.hailtypedisplay8,
	quickhail.ui.hailtypedisplay9,
	quickhail.ui.hailtypedisplay10
}
quickhail.ui.displaylinehail = iup.vbox{
	quickhail.ui.haildisplay1,
	quickhail.ui.haildisplay2,
	quickhail.ui.haildisplay3,
	quickhail.ui.haildisplay4,
	quickhail.ui.haildisplay5,
	quickhail.ui.haildisplay6,
	quickhail.ui.haildisplay7,
	quickhail.ui.haildisplay8,
	quickhail.ui.haildisplay9,
	quickhail.ui.haildisplay10
}

quickhail.ui.haillist = iup.vbox{
	quickhail.ui.hailtitle,
	iup.hbox{
		quickhail.ui.displaylinenum,
		quickhail.ui.displaylinetype,
		quickhail.ui.displaylinehail
	}
}

quickhail.ui.main = iup.dialog{
	quickhail.ui.display,
    TITLE = nil,
	BORDER = 'NO',
	TOPMOST = 'NO',
	RESIZE = 'NO',
	MAXBOX = 'NO',
	MINBOX = 'NO',
	MODAL = 'YES',
	FULLSCREEN = 'NO',
	EXPAND = 'YES',
	ACTIVE = 'NO',
	MENUBOX = 'NO',
	BGCOLOR = "0 0 0 0 *",
	SIZE = "500x500"
}

function quickhail.ui.hailtypeset()
	for i=1,10 do
		if quickhail.var['hailtype'..i] == 'msg %target%' then
			quickhail.ui['hailtypedisplay'..i].title = '<PM>'
			quickhail.ui['hailtypedisplay'..i].fgcolor = '255 000 000'
		elseif quickhail.var['hailtype'..i] == 'say_sector' then
			quickhail.ui['hailtypedisplay'..i].title = '<SC>'
			quickhail.ui['hailtypedisplay'..i].fgcolor = '000 255 000'
		end
	end
end

function quickhail.display.qhliston()
	if quickhail.var.ondisplay ~= nil then
		quickhail.display.detachall()
	end
	if quickhail.ui.ondisplay ~= 'haillist' then
		iup.Append(quickhail.ui.display, quickhail.ui.haillist)
		quickhail.var.ondisplay = 'haillist'
	end
end

function quickhail.showmain()
	if quickhail.ui.active == false then
		quickhail.ui.main:showxy((quickhail.var.x/100)*gkinterface.GetXResolution(), (quickhail.var.y/100)*gkinterface.GetYResolution())
		quickhail.ui.active = true
		quickhail.setkeys()
	end
end

function quickhail.hidemain()
	if quickhail.ui.active == true then
		quickhail.ui.main:hide()
		quickhail.ui.active = false
		quickhail.resetkeys()
		quickhail.display.detachall()
	end
end

function quickhail.display.detachall()
	if quickhail.ui.ondisplay == 'haillist' then
		iup.Detach(quickhail.ui.haillist, quickhail.ui.display)
		quickhail.var.ondisplay = nil
	end
end

-- END DISPLAY UI

-- START CONFIG UI

-- Elements

for i=1,10 do
	quickhail.ui["hailname"..i] = iup.text{ value = quickhail.var["hail"..i], size="650x" }
	quickhail.ui["hailtype"..i] = iup.stationtoggle{
		title = "Sector", 
		value = 'OFF',
		action = function(self, v)
			if(v == 1) then
				quickhail.var['hailtypetf'..i] = 'ON'
			else
				quickhail.var['hailtypetf'..i] = 'OFF'
			end
		end
	}
	quickhail.ui["hail"..i] = iup.hbox {iup.label{title = "hail "..i.."  " }, quickhail.ui["hailname"..i], quickhail.ui["hailtype"..i]}
end

quickhail.ui.saveb = iup.stationbutton{title = "Save"}
quickhail.ui.closeb = iup.stationbutton{title = "Close"}
quickhail.ui.cancelb = iup.stationbutton{title = "Cancel"}
quickhail.ui.xpos = iup.text{value = (quickhail.var.x), size="60x"}
quickhail.ui.ypos = iup.text{value = (quickhail.var.y), size="60x"}
quickhail.ui.font = iup.text{value = quickhail.var.font, size="60x"}

quickhail.ui.config = iup.dialog {
	iup.pdarootframe {
		iup.vbox {
			iup.label{title = "Hail Config"},
			quickhail.ui.hail1,
			quickhail.ui.hail2,
			quickhail.ui.hail3,
			quickhail.ui.hail4,
			quickhail.ui.hail5,
			quickhail.ui.hail6,
			quickhail.ui.hail7,
			quickhail.ui.hail8,
			quickhail.ui.hail9,
			quickhail.ui.hail10,
			iup.hbox{iup.label{title = "Dialog Position: X "}, quickhail.ui.xpos,iup.label{title = " Y "}, quickhail.ui.ypos, iup.label{title = "   Font Size: "}, quickhail.ui.font},
			iup.hbox{quickhail.ui.saveb, quickhail.ui.closeb, quickhail.ui.cancelb}
		}
	};
	TITLE = "QuickHail v"..quickhail.version,
	TOPMOST = "yes"
}

function quickhail.ui.saveb:action()
	for i=1,10 do
		if quickhail.var['hailtypetf'..i] == 'ON' then
			quickhail.var['hailtype'..i] = 'say_sector'
		else
			quickhail.var['hailtype'..i] = 'msg %target%'
		end
		local key = quickhail.charid.."hailtype"..i
		local val = quickhail.var['hailtype'..i]
		quickhail.settingslist[key] = val
	end
	quickhail.var.x = tonumber(iup.GetAttribute(quickhail.ui.xpos, "value"))
	quickhail.settingslist['UIXpos'] = quickhail.var.x
	quickhail.var.y = tonumber(iup.GetAttribute(quickhail.ui.ypos, "value"))
	quickhail.settingslist['UIYpos'] = quickhail.var.y
	quickhail.var.font = tonumber(iup.GetAttribute(quickhail.ui.font, "value"))
	quickhail.settingslist['font'] = quickhail.var.font
	for i=1,10 do
		quickhail.var["hail"..i] = iup.GetAttribute(quickhail.ui['hailname'..i], "value")
		local key = quickhail.charid .. "hail" .. i
		local val = quickhail.var["hail" .. i]
		quickhail.settingslist[key] = val
		quickhail.ui['haildisplay' .. i].font = quickhail.var.font
		quickhail.ui['hailnumdisplay' .. i].font = quickhail.var.font
		quickhail.ui['hailtypedisplay'..i].font = quickhail.var.font
	end
	quickhail.ui.hailtitle.font = quickhail.var.font
	quickhail.loadvarscript()
	quickhail.updatenotes()
end

function quickhail.ui.closeb:action()
	quickhail.hidecfg()
end

function quickhail.ui.cancelb:action()
	for i=1,10 do
		if quickhail.var["hailtype"..i] == 'say_sector' then
			quickhail.ui["hailtype"..i].value = 'ON'
		elseif quickhail.var["hailtype"..i] == 'msg %target%' then
			quickhail.ui["hailtype"..i].value = 'OFF'
		end
		local key = quickhail.charid..'hail'..i
		local val = "Hail!"
		quickhail.var['hail'..i] = quickhail.settingslist[key] or val
		quickhail.ui['hailname'..i].value = quickhail.var['hail'..i]
	end
	quickhail.ui.xpos.value = quickhail.var.x
	quickhail.ui.ypos.value = quickhail.var.y
	quickhail.ui.font.value = quickhail.var.font
end


function quickhail.showcfg()
	do
	quickhail.loadvariables()
	end
	quickhail.ui.config:show()
end

function quickhail.hidecfg()
	quickhail.ui.config:hide()
end

RegisterUserCommand('quickhail', quickhail.showcfg)
RegisterUserCommand('qh', quickhail.showcfg)

-- END CONFIG UI