-- Global
-- quickhail 274826482-10



quickhail.ui = {}
quickhail.ui.active = false
quickhail.ui.display = iup.vbox{}

quickhail.var = {}
quickhail.var.ondisplay = {}

quickhail.var.font = quickhail.settingslist['font'] or 15 
quickhail.var.x = quickhail.settingslist['UIXpos'] or 14.5
quickhail.var.y = quickhail.settingslist['UIYpos'] or 18

quickhail.currentmode = ''

quickhail.display = {}

-- Binds
quickhail.keyvar = {}
quickhail.default = {}
for i=0,9 do quickhail.keyvar[i] = {} end

-- Hail Script
for i=1,10 do
	quickhail.var['hail'..i] = ""
	quickhail.var['hailtype'..i] = ""
	quickhail.var['hailnum'..i] = ""
end

quickhail.charid = nil
quickhail.var.hailsloaded = 'no'